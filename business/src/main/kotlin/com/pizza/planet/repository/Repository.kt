package com.pizza.planet.repository

interface Repository {

    fun getQueue(): ArrayList<Int>

    fun addToQueue(orderId: Int)

    fun removeFromQueue(orderId: Int)

    fun getPizzaInQueue(): Int

    fun getPizzaInProgress(): Int

    fun getPizzaDone(): Int

    fun getAvailableWorkers(): Int

    fun cooking()

    fun finished()

    fun newOrder(): Int

}
