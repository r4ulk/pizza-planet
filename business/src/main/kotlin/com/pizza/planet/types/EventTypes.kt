package com.pizza.planet.types

enum class EventTypes(val fullName: String) {
    COOKING("Starting cooking"),
    FINISHED("Cooking is finished"),
    NEW_WORKER_AVAILABLE("New Worker Available"),
    WORKER_AVAILABLE("Count of Worker Available"),
    PIZZA_IN_PROGRESS("Count of Pizza in Progress"),
    PIZZA_IN_QUEUE("Count of Pizza in Queue"),
    PIZZA_DONE("Count of Pizza done!")

}
