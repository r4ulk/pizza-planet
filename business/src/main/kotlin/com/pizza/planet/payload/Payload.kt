package com.pizza.planet.payload

import com.pizza.planet.types.EventTypes
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf

object Payload{

  fun build(event: EventTypes, key: String, value: Int): JsonObject {
      return jsonObjectOf(
        "event" to event.toString(),
        "payload" to JsonObject("{ \"${key}\": \"${value}\"}" )
      )
  }

}
