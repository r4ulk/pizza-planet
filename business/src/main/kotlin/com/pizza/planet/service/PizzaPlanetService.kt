package com.pizza.planet.service

import com.pizza.planet.payload.Payload
import com.pizza.planet.repository.EventBus
import com.pizza.planet.repository.Repository
import com.pizza.planet.types.EventTypes
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import org.slf4j.LoggerFactory
import kotlinx.coroutines.*

class PizzaPlanetService(private val repository: Repository) {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun create(ctx: RoutingContext) = runBlocking {
        logger.info("Creating new Pizza Planet request")
        launch {
          publish(ctx, EventTypes.COOKING, "id", repository.newOrder())
          publish(ctx, EventTypes.PIZZA_IN_QUEUE, "count", repository.getPizzaInQueue())
        }
    }

    fun addToQueue(orderId: Int) {
      logger.info("Your Pizza Order: ${orderId} is waiting for Available Worker")
      repository.addToQueue(orderId)
    }

    fun removeFromQueue(orderId: Int) {
      repository.removeFromQueue(orderId)
    }

    fun getOrderId(message: Message<JsonObject>): Int {
      val payload = message?.body()?.getJsonObject("payload")
      return Integer.parseInt(payload?.getValue("id").toString())
    }

    private fun publish(ctx: RoutingContext, event: EventTypes, key: String, value: Int){
      ctx.vertx().eventBus().publish(EventBus.address, Payload.build(event, key, value))
    }
}
