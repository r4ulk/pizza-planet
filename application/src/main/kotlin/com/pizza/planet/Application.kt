package com.pizza.planet

import com.pizza.planet.adapters.db.WorkerRepository
import com.pizza.planet.adapters.event.EventVerticle
import com.pizza.planet.adapters.http.HttpAdapter
import com.pizza.planet.config.ConfigLoad
import com.pizza.planet.service.PizzaPlanetService
import io.vertx.core.DeploymentOptions
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.json.JsonObject
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

fun main() {
    System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory")

    val vertxOptions = VertxOptions(JsonObject().put("maxEventLoopExecuteTime", TimeUnit.SECONDS.toNanos(60)))
    val vertx = Vertx.vertx(vertxOptions)
    val logger = LoggerFactory.getLogger("com.pizza.planet.Application")
    val options = DeploymentOptions()

    ConfigLoad().config(vertx)
        .compose { json ->
            options.config = json
            Future.succeededFuture<Unit>()
        }
        .compose { Future.future<String> { p -> vertx.deployVerticle(EventVerticle(PizzaPlanetService(WorkerRepository)), options, p) } }
        .compose { Future.future<String> { p -> vertx.deployVerticle(HttpAdapter(), options, p) } }
        .onSuccess { logger.info("Pizza Planet has been started.") }
        .onFailure { err ->
            logger.error(err.message)
            exitProcess(1)
        }
}
