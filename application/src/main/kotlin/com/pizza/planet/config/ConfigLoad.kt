package com.pizza.planet.config

import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

class ConfigLoad {

    fun config(vertx: Vertx): Future<JsonObject> {
        val envStore = ConfigStoreOptions()
        envStore.let {
            it.type = "env"
        }

        val fileStore = ConfigStoreOptions()
        fileStore.let {
            it.type = "file"
            it.format = "yaml"
            it.config = JsonObject().put("path", "application.yml")
        }

        val options = ConfigRetrieverOptions()
        options.let {
            it.addStore(envStore)
            it.addStore(fileStore)
        }

        val retriever = ConfigRetriever.create(vertx, options)
        return Future.future { promise -> retriever.getConfig(promise) }
    }

}
