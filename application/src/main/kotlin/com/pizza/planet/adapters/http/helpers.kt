package com.pizza.planet.adapters.http

import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf

const val JSON_TYPE = "application/json"

fun HttpServerResponse.created(message: String) {
    this.putHeader("Content-Type", JSON_TYPE)
    this.statusCode = 201
    this.end(jsonObjectOf("status" to "created", "message" to message).toBuffer())
}

fun HttpServerResponse.deleted() {
    this.putHeader("Content-Type", JSON_TYPE)
    this.statusCode = 204
    this.end()
}

fun HttpServerResponse.json(value: JsonObject) {
    this.putHeader("Content-Type", JSON_TYPE)
    this.end(value.toBuffer())
}

fun HttpServerResponse.json(value: Any) {
    this.putHeader("Content-Type", JSON_TYPE)
    this.end(Json.encode(value))
}

fun HttpServerResponse.notFound(value: String) {
    this.putHeader("Content-Type", JSON_TYPE)
    this.statusCode = 404
    this.end(jsonObjectOf("status" to "not_found", "message" to value).toBuffer())
}
