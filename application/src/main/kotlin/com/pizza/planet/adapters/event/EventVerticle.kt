package com.pizza.planet.adapters.event

import com.pizza.planet.adapters.db.WorkerRepository
import com.pizza.planet.payload.Payload
import com.pizza.planet.repository.EventBus
import com.pizza.planet.service.PizzaPlanetService
import com.pizza.planet.types.EventTypes
import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.core.logging.Logger
import io.vertx.core.logging.LoggerFactory
import kotlinx.coroutines.*

class EventVerticle(private val service: PizzaPlanetService): AbstractVerticle() {
    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    override fun start(start: Promise<Void>) {
        logger.info("EventVerticle module initialized.")

        vertx
          .eventBus()
          .consumer<JsonObject>(EventBus.address)
          .handler { msg ->
            val event = EventTypes.valueOf(
              msg.body().getString("event")
            )
            when(event) {
              EventTypes.COOKING -> cookHandler(msg)
              EventTypes.FINISHED -> finishedHandler(msg)
              EventTypes.NEW_WORKER_AVAILABLE -> workerAvailableHandle()
            }

        }
        start.complete()
    }

    private fun cookHandler(msg: Message<JsonObject>) {
        val availableWorkers = WorkerRepository.getAvailableWorkers()
        runBlocking { // this: CoroutineScope
            val orderId = service.getOrderId(msg)
            if(availableWorkers > 0) {
                launch { cook(orderId, availableWorkers) }
            }else {
                service.addToQueue(orderId)
            }
        }
    }

    private suspend fun cook(orderId: Int, availableWorkers: Int) {
        logger.info("AVAILABLE_WORKERS: ${availableWorkers}")
        runCatching {
          logger.info("We're Cooking your Pizza! Order: ${orderId}")
          WorkerRepository.cooking()
          publish(EventTypes.WORKER_AVAILABLE,"count", WorkerRepository.getAvailableWorkers())
          publish(EventTypes.PIZZA_IN_PROGRESS,"count", WorkerRepository.getPizzaInProgress())
          publish(EventTypes.PIZZA_IN_QUEUE,"count", WorkerRepository.getPizzaInQueue())
          delay(10000L)
          publish(EventTypes.FINISHED,"id", orderId)
        }.onFailure { err ->
          logger.error("Fail to cook your pizza! err: ${err.message}")
        }
    }

    private fun finishedHandler(msg: Message<JsonObject>) {
        val orderId = service.getOrderId(msg)
        WorkerRepository.finished()
        logger.info("Your Pizza Order: $orderId is done. Taste it!")
        publish(EventTypes.NEW_WORKER_AVAILABLE,"count", WorkerRepository.getAvailableWorkers())
        publish(EventTypes.PIZZA_IN_PROGRESS,"count", WorkerRepository.getPizzaInProgress())
        publish(EventTypes.PIZZA_DONE,"count", WorkerRepository.getPizzaDone())
    }

    private fun workerAvailableHandle() {
        val availableWorkers = WorkerRepository.getAvailableWorkers()
        runBlocking {
            val queue = WorkerRepository.getQueue()
            if (queue.isNotEmpty()) {
                val orderId = queue[0]
                launch { cook(orderId, availableWorkers) }
                service.removeFromQueue(orderId)
            }
        }
    }

    private fun publish(event: EventTypes, key: String, value: Int){
      vertx.eventBus().publish(EventBus.address, Payload.build(event, key, value))
    }

}

