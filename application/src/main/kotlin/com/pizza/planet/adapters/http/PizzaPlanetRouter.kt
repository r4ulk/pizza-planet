package com.pizza.planet.adapters.http

import com.pizza.planet.repository.EventBus
import com.pizza.planet.service.PizzaPlanetService
import io.vertx.core.Vertx
import io.vertx.ext.bridge.BridgeEventType
import io.vertx.ext.bridge.PermittedOptions
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions
import io.vertx.ext.web.handler.sockjs.SockJSHandler
import io.vertx.kotlin.core.json.jsonObjectOf

class PizzaPlanetRouter(private val service: PizzaPlanetService) {

    fun registerRoutes(vertx: Vertx): Router {
        val router = Router.router(vertx)

        // To Use SockJS instead EventBus
        //val options: SockJSHandlerOptions = SockJSHandlerOptions().setRegisterWriteHandler(true)
        //val sockJSHandler = SockJSHandler.create(vertx, options)

        val sockJSHandler = SockJSHandler.create(vertx)
        router.also {
          it.post("/").blockingHandler(this::create)
          it.mountSubRouter("/eventbus", eventBusHandler(sockJSHandler) )
        }

        return router
    }

  /**
   * creates a new pizza request
   */
  private fun create(ctx: RoutingContext) {
      service.create(ctx)
      ctx
        .response()
        .json(
          jsonObjectOf(
            "status" to "OK",
            "message" to "Pizza Request created successfully.",
            "data" to jsonObjectOf("flowDefId" to 123)))
  }

  private fun eventBusHandler(sockJSHandler: SockJSHandler): Router? {
      val options: SockJSBridgeOptions = SockJSBridgeOptions()
        .addInboundPermitted(PermittedOptions().setAddress(EventBus.address))
        .addOutboundPermitted(PermittedOptions().setAddress(EventBus.address))
      return sockJSHandler.bridge(options) { event ->
        if (event.type() == BridgeEventType.SOCKET_CREATED) {
          println("A socket was created for Address: ${EventBus.address}");
        }
        event.complete(true)
      }
  }

  private fun sockJSHandler(sockJSHandler: SockJSHandler, vertx: Vertx): Router? {
      return sockJSHandler.socketHandler{ sockJSHandler ->
        val writeHandlerID: String = sockJSHandler.writeHandlerID()
        EventBus.handlerID = writeHandlerID
        println(writeHandlerID)
      }
  }

}
