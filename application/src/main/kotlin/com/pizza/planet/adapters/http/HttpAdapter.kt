package com.pizza.planet.adapters.http

import com.pizza.planet.adapters.db.WorkerRepository
import com.pizza.planet.service.PizzaPlanetService
import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.*
import io.vertx.kotlin.core.json.jsonObjectOf
import org.slf4j.LoggerFactory

class HttpAdapter : AbstractVerticle() {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun start(start: Promise<Void>) {
        val server = config().getJsonObject("server")

        val pizzaPlanetRouter = PizzaPlanetRouter(PizzaPlanetService(WorkerRepository))

        val router = Router.router(vertx)

        router.also {
            it.route().handler(BodyHandler.create())
            it.route().handler(createCors())
            it.route("/docs/*").handler(StaticHandler.create("docs"))
            it.route().handler(LoggerHandler.create(LoggerFormat.TINY))
            it.route().handler(TimeoutHandler.create(7500))
            it.route().handler(this::securityHandler)
            it.route().failureHandler(this::errorHandler)
            it.route().handler(staticHandler());
            it.get("/health").handler(this::healthHandler)
            it.mountSubRouter("/v1/pizza-planet", pizzaPlanetRouter.registerRoutes(vertx))
        }

        val serverPort = server.getInteger("port")

        vertx.createHttpServer()
            .requestHandler(router)
            .listen(serverPort, server.getString("host")) { res ->
                if (res.failed()) {
                    start.fail(res.cause())
                    return@listen
                }

                logger.info("Http module is ready on port $serverPort!")
                start.complete()
            }
    }

    private fun errorHandler(ctx: RoutingContext) {
        val failure = ctx.failure()

        logger.warn(failure.message)
        failure.printStackTrace()
        ctx.response().setStatusCode(400).json(jsonObjectOf("status" to "fail", "message" to failure.message))
    }

    private fun createCors(): CorsHandler {
        val handler = CorsHandler.create("*")
        handler.allowedMethods(mutableSetOf(HttpMethod.OPTIONS, HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE, HttpMethod.PATCH))
        handler.allowedHeaders(mutableSetOf("Content-Type", "Content-Length", "Authorization"))
        return handler
    }

    private fun healthHandler(ctx: RoutingContext) {
        val version = config().getJsonObject("service").getString("version")
        ctx.response().setStatusCode(200)
            .setStatusMessage("Healthy")
            .json(jsonObjectOf("status" to "OK", "version" to version))
    }

    private fun securityHandler(ctx: RoutingContext) {
        val token = ctx.request().getHeader("Authorization")
        val isPreFlight = ctx.request().method() == HttpMethod.OPTIONS
        val isHealthPath = ("/health" == ctx.request().path() || "/favicon.ico" == ctx.request().path())
        val isDocPath = ctx.request().path().startsWith("/docs")
        val isSecurePath = (!isPreFlight && !isHealthPath && !isDocPath)

        ctx.next()
    }

    private fun staticHandler(): StaticHandler? {
        return StaticHandler.create().setCachingEnabled(false)
    }

}
