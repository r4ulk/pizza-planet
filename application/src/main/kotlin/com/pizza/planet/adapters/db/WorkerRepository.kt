package com.pizza.planet.adapters.db

import com.pizza.planet.repository.Repository

object WorkerRepository : Repository {

    private var orderNumber = 0 // Order number incremental generator
    private var availableWorker = 3 // Total of workers available
    private var pizzaInProgress = 0 // Total of pizza in the oven
    private var pizzaInQueue = 0 // Total of pizza in the queue
    private var pizzaDone = 0 // Total of pizza done
    private var queue = arrayListOf<Int>() // Store the orderId in the queue list

    override fun getQueue(): ArrayList<Int> {
      return queue
    }

    override fun addToQueue(orderId: Int) {
      queue.add(orderId)
    }

    override fun removeFromQueue(orderId: Int) {
      queue.remove(orderId)
    }

    override fun getAvailableWorkers(): Int {
      return availableWorker
    }

    override fun getPizzaInProgress(): Int {
      return pizzaInProgress
    }

    override fun getPizzaInQueue(): Int {
      return pizzaInQueue
    }

  override fun getPizzaDone(): Int {
    return pizzaDone
  }

    override fun cooking() {
      if(availableWorker > 0) {
        availableWorker--
        pizzaInProgress++
        pizzaInQueue--
      }
    }

    override fun finished() {
      if(availableWorker < 3) {
        availableWorker++
        pizzaInProgress--
        pizzaDone++
      }
    }

    override fun newOrder(): Int {
      orderNumber++
      pizzaInQueue++
      return orderNumber
    }

}
