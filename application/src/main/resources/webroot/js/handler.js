
/*
  Register the Event-Bus handler for the Frontend Web-browser
*/
function registerSocketJSHandler() {
    var sock = new SockJS('http://localhost:8080/v1/pizza-planet/eventbus');
    sock.onopen = () => {
        console.log('websocket connection opened');
    };

    sock.onmessage = (message) => {
        console.log('message: ' + message?.data?.body);
    };

    sock.onevent = (event, message) => {
        console.log('event: %o, message:%o', event, message);
        return true; // in order to signal that the message has been processed
    };

    sock.onunhandled = (json) => {
        console.log('this message has no address:', json);
    };

    sock.onclose = () => {
        console.log('close');
    };
};

/*
  Register the Event-Bus handler for the Frontend Web-browser
*/
function registerHandler() {
    var eventBus = new EventBus('http://localhost:8080/v1/pizza-planet/eventbus');
    eventBus.onopen = () => {
        eventBus.registerHandler('pizza-worker', (error, message) => {
            console.log('received a message: ' + JSON.stringify(message?.body));
            messageHandler(message);
        });
    }
};

/*
  Message received Handler
*/
function messageHandler(message) {
  if(message == null) return;
  if(message.body.event == "WORKER_AVAILABLE" ||
      message.body.event == "NEW_WORKER_AVAILABLE"){
    document.getElementById('workers').innerHTML = message.body.payload.count;
  }
  if(message.body.event == "PIZZA_IN_PROGRESS"){
    document.getElementById('inprogress').innerHTML = message.body.payload.count;
  }
  if(message.body.event == "PIZZA_IN_QUEUE"){
    document.getElementById('inqueue').innerHTML = message.body.payload.count;
  }
  if(message.body.event == "PIZZA_DONE"){
    document.getElementById('done').innerHTML = message.body.payload.count;
  }
};

/*
  Creates a new pizza order request
*/
function pizza() {
    var xmlhttp = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status != 200) {
                  alert('error')
                }
            }
        };
        xmlhttp.open("POST", "http://localhost:8080/v1/pizza-planet");
        xmlhttp.setRequestHeader("Content-Type", "application/json");
        xmlhttp.send();
}



